#Actes de Caliu 2024

## Reunió de gener

13 de gener de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Josep M. Ferrer, Aniol Martí. S'informa que els masovers han actualitzat el servidor de miralls a Debian 12. Ja només en queden 2 per actualitzar.

## Reunió de febrer

3 de febrer de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Miquel Adroer, Josep M. Ferrer, Aniol Martí. Quedem a l'estació del cremallera de Núria a les 12:00 del dia 24 de febrer. Dinarem a l'hotel a les 14:00 i després farem l'Assemblea General i repartirem les tasses commemoratives.

## Assemblea General

24 de febrer de 2024, Núria, Ripollès. Assistents: Walter Garcia, Mercè, Miquel Adroer, Rafael Carreras, Núria, Alex Muntada, Aniol Martí, Josep M. Ferrer, ++++, Jordi Funollet. El Miquel reparteix les tasses i passegem per la zona. Dinem al restaurant i es presenten les feines de Caliu, dels masovers i els comptes. Es fa un agraïment a l'Orestes pels discos per al servidor cedits a Caliu. S'agraeix a l'Alex la feina feta amb els masovers, es recorda que s'ha de corregir l'informe de masovers per uns DNS, s'avisarà en David. Es prorroguen les eleccions a la junta al 2025.

## Reunió d'abril

6 d'abril de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Josep M. Ferrer, Walter Garcia, +++++, Rafael Carreras. Es comenta l'organització del Dia de la Llibertat de Maquinari a celebrar el dia 27 d'abril. Es contactaran més ponents, hi ha una xerrada conformada. Es comenten notícies de seguretat informàtica i la possibilitat d'incorporar nous miralls al servidor.

# Reunió de maig

4 de maig de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Walter Garcia, David Pinilla. Es valora positivament el Dia de la Llibertat de Maquinari a celebrar el dia 27 d'abril. Van venir unes 22 persones. El 21 de setembre celebrarem el Dia de la Llibertat del Programari, en principi a Bocanord. Es detecta un error a l'SMART a un disc del servidor. Sembla que és puntual, però es monitoritzaran més els errors del servidor.

# Reunió de setembre

7 de setembre de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: David Pinilla, Rafael Carreras, Walter Garcia, Josep M. Ferrer, Aniol Martí. La UPC ha cedit una màquina que anirà enrackada i s'han de comprar dos adaptadors RAID (David) i dos discos durs (Josep Maria). A partir del dia 17, l'Aniol començarà a muntar el nou servidor. El 21 de setembre celebrarem el Dia de la Llibertat del Programari a Bocanord. Es tanca la graella de xerrades i s'actualitzarà el web i el cartell de l'esdeveniment per fer-ne difusió.

# Reunió d'octubre

5 d'octubre de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Walter Garcia, Rafael Carreras, Josep M. Ferrer, Aniol Martí, Ramon. Es valora positivament el Dia de la Llibertat del Programari a Bocanord. Es troben a faltar més ponents per tal de fer l'esdeveniment més interessant, però no surten idees per aconseguir-ne més. El servidor de miralls ja està en marxa des de fa una hora o així. Es retorna el disc dur defectuós i se n'encarrega un altre de 8 GB que es desarà fins tenir.ne rous per canviar-los tots. Es pot moure ja al CPD de la UPC però ja es veurà si es fa aviat o no. S'ha de canviar l'enllaç al twitter desaparegt de Caliu pel de Mastodon i, si pot ser, també el logotip. Els masovers estudiaran la possibilitat de contractar un servei de backup que serviria bàsicament per la pàgina web.

# Reunió de desembre

7 de novembre de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Walter Garcia, Rafael Carreras, Josep M. Ferrer. Excusat: Aniol Martí. Es canviarà de lloc el servidor, es mirarà de fer a partir de febrer. S'ha de renovar el domini, però encara falta bastant. La propera reunió es farà el 18 de gener. Mirarem de fer el Dia de la Llibertat del Maquinari el 26 d'abril.