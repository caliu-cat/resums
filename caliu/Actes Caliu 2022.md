# Actes de Caliu 2022

## Reunió de gener de 2022
Dissabte 8 de gener de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, David Pinilla, Xavier de Pedro, Aniol Martí, Josep M. Ferrer, Orestes Mas, Alex Muntada. No hi ha cap més notícia de la instància enviada. S'enviarà un correu de reclamació. Es parla d'una nova iniciativa de l'Ajuntament de Barcelona i Xnet, bàsicament amb 5 escoles, per implantar serveis lliures (Moodle, Nexcloud,...). Es contactarà gent per celebrar el Dia de la Llibertat del Maquinari a l'abril. S'ha d'actualitzar el PHP del servidor de Caliu, en principi al febrer. S'hauria d'actualitzar Bullseye. Es proposa de fer servir Netdata.cloud (programari lliure al núvol) per monitoritzar el servidor.

## Reunió de febrer de 2022
Dissabte 5 de febrer de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: David Pinilla, Aniol Martí, Josep M. Ferrer, Alex Muntada. Es contacta un espai per celebrar el Dia de la Llibertat del Maquinari a l'abril, sense èxit. S'actualitza el servidor i es troben problemes amb els Mediawiki que hi ha.

## Reunió de març de 2022
Dissabte 5 de març de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: David Pinilla, Josep M. Ferrer, Xavier de Pedro, Miquel Chicano, Rafael Carreras. El Xavier anirà a parlar amb Bocanord per veure per què no responen els correus. Es contactaran altres llocs per temptejar, i es mirarà d'organitzar el DLM. Se cercaràtemps per reparar els Mediawiki del servidor. L'Administració ha respost quan se'ls ha contactat mitjançant un formulari web i han respost que són conscients del problema amb els formularis no acceccibles amb programari lliure i que estan treballant en la migració a HTML5. Es constata que ja s'han migrat alguns, el problema és que en són molts. Prioritzen els més usats.

## Reunió d'abril de 2022
Dissabte 2 d'abril de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Xavier de Pedro. Es dedideix esperar un parell de mesos abans de tornar a contactar la Generalitat per tal de fer el seguiment de les feines de migració de formularis a HTML. Es mirarà de contactar els masovers per conèixer les tasques pendents. S'han contactat alguns espais per celebrar el Dia de la Llibertat del Maquinari al maig. Es decidirà en breu. S'apunta un nou lloc amb moltes possibilitats, també se'ls contactarà.

## Reunió de maig de 2022
Dissabte 7 de maig de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Aniol Martí. Es mirarà de contactar els masovers per conèixer les tasques pendents. Se celebrarà el Dia de la Llibertat del Maquinari dissabte 14 de maig a Gràcia, Barcelona. La graella ja està tancada. Es contactaran els vocals de Caliu per veure si volen presentar-se a la candidatura. Es contactarà el Xavier de Pedro per veure si hi vol entrar com a vocal. Es reparteixen les feines per l'Assemblea General de juny.

## Assemblea General de 2022
Dissabte 4 de juny, Palau Robert de Barcelona. Assistents: Josep M. Ferrer, Aniol Martí, Francesc Vilaubí, Alex Muntada, Miquel Chicano, Orestes Mas. Es ratifica la composició de la junta que queda com estava.

## Reunió de juliol de 2022
Dissabte 2 de juliol, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Aniol Martí.

## Reunió de setembre de 2022
Dissabte 3 de setembre, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Aniol Martí. Es decideix que hem de quedar per poder portar els papers de canvi de junta a Justícia, signats. La propera reunió es farà presencial. S'acaba de preparar el Dia de la Llibertat del Programari aquest any de nou a Bocanord.

## Reunió d'octubre de 2022
Dissabte 15 d'octubre, Interior de te, Gràcia, Barcelona. Assistents: Rafael Carreras, Alex Muntada, Aniol Martí. Es valora positivament el Dia de la Llibertat del Programari. Com que ja no es pot presentar en persona la renovació de la junta i com que la Generalitat encara fa servir formularis privatius, es decideix que la farem d'alguna manera des de la feina. Els fons europeus Next Generation han de servir també per a la migració del sistema privatiu dels formularis.

## Reunió de desembre de 2022
Dissabte 3 de desembre, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Aniol Martí, Orestes Mas, David Pinilla. Es decideix canviar la data de la propera reunió. Es decideix fer l'Assemblea General al febrer, a un lloc a determinar a finals del mes de desembre.
