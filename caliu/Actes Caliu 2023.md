#Actes de Caliu 2023

## Reunió de gener
14 de gener de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Miquel Adroer, Josep M. Ferrer, Alex Muntada, Aniol Martí, David Pinilla, Walter García, Orestes Mas. Mirarem de celebrar el Dia de la Llibertat del Maquinari a Gràcia a l'abril. Acceptarem com a socis l'associació que ens ho ha demanat, amb la quota normal. Organitzem l'Assemblea General de febrer: cafè i Assemblea al Casal a les 11:00, tomb pel poble, dinar a l'Arts.

## Assemblea General
25 de febrer de 2023, Casal de l'Espluga de Francolí. Assistents: Miquel Adroer, Vicky Mascaró, Pau Funollet, Jordi Funollet, Eva Perales, Francesc Vilaubí, Josep M. Ferrer, Orestes Mas, Ana Pastor, Aniol Martí, Rafael Carreras. Aprovem l'acta anterior i els comptes de Caliu. La propera reunió serà el 25 de març. S'afegirà un agraïment a l'Alex Muntada per les feines de masover.

## Reunió de març
25 de març de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Alex Muntada, Aniol Martí, David Pinilla, Orestes Mas. Valorem l'Assemblea i la visita cultural. Comentem el Dia de la Llibertat del Maquinari. A l'abril, els masovers  faran feines de manteniment del servidor. La propera reunió serà al maig.

## Reunió de maig
5 de maig de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Aniol Martí, David Pinilla, Josep M. Ferrer, Miquel Adroer. Valorem positivament el Dia de la Llibertat del Maquinari. Queda pendent una actualització al servidor.

## Reunió de juny
3 de juny de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Aniol Martí. Es programen les feines pel Dia de la Llibertat del Programari. Es parla dels problemes amb els discos durs nous del servidor, no sabem què els hi passa. Es tornarà a mirar i es farà l'actualització pendent. Al juliol començarem a parlar de la celebració del 25è aniversari de Caliu.

## Reunió de juliol
1 de juliol de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Miquel Adroer, Rafael Carreras, Josep M. Ferrer, Ramon Collet. El Dia de la Llibertat del Programari el celebrarem a Bocanord. S'ha de fer transferència a l'Orestes pels discos durs nous del servidor. La celebració del 25è aniversari de Caliu la farem el 24 de febrer de 2024 a Núria. Es mirarà de fer unes tasses commemoratives. Es fa la crida a la participació pel DLP.

## Reunió de setembre
2 de setembre de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer. El Dia de la Llibertat del Programari el celebrarem a Bocanord. Hi haurà dos ponents. Es preguntarà a la junta quan donem de baixa el compte de Caliu de Twitter.

## Reunió d'octubre
7 d'octubre de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer. Excusa la seva assistència l'Orestes Mas. Ja està acabat el disseny per la tassa del 25è aniversari de Caliu. Es mirarà de fer un correu a la llista per saber quanta gent en vol. Es valora positivament el Dia de la Llibertat del Programari. S'esborra el compte de Caliu de Twitter. Fem seguiment de les tasques dels masovers, avisarem l'Aniol. Es fa una actualització d'un dels servidors a la reunió.

## Reunió de novembre
4 de novembre de 2023, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Josep M. Ferrer, Aniol Martí, Walter García, Miquel Adroer. Excusa la seva assistència l'Alex Muntada. Ja tenim pressupost per les tasses. Es farà un correu a la llista per veure quanta gent hi està interessada i mirarem quantes en demanem. Deixarem de termini fins a finals de mes. Fem seguiment de les tasques dels masovers. Queda feina d'actualitzacions. Farem un correu a la llista per reunió presencial a la Noguera.

## Reunió de desembre
2 de desembre de 2023, Montargull (la Noguera). Assistents: Rafael Carreras, Walter García, Miquel Adroer, Alex Muntada. Ens han encarregat 15 tasses conmemoratives, però en demanarem 25 per tenir-ne de reserva. Es parla de les actualitzacions pendents de les màquines de Caliu i es planificarà alguna per aquest mes. Es farà una crida per saber quanta gent vindrà a Núria. Després es reservarà per dinar.
