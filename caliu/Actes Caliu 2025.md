# Actes de Caliu 2025

## Reunió de gener
18 de gener de 2024, canal Jitsi #caliu a meet.guifi.net. Assistents: Rafael Carreras, Aniol Martí, Josep M. Ferrer, David Pinilla. Farem l'Assemblea General i eleccions a la junta l'1 de març, a un lloc per decidir a la llista. El Josep M. farà la renovació del domini. El Josep M. reservarà una sala a la Fontana per al DLM el 26 d'abril.
